module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                files: {
                    'styles/css/style.css': 'styles/sass/style.scss'
                }
            }
        },
        watch: {
            sass: {
                files: '**/*.scss',
                tasks: ['sass']

            }
        }

    });

    // Load Grunt plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    //register tasks
    grunt.registerTask('default', ['watch']);

};
