var dataFeed = angular.module("dataFeed", ['ngRoute', 'ui.sortable']);

dataFeed.config(function($locationProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

});

dataFeed.filter("dateFilter", function() {
    return function(item) {
        if (item != null) {
            return new Date(item);
        }
        return "";
    }
});


dataFeed.controller('FeedCtrl', function($scope, $http, $location) {

    $http.get('todo.json').success(function(data) {
        $scope.information = data;
    });

    $scope.user = $location.hash();
    if ($scope.user == '') {
        $scope.user = 1;
    }

    $scope.show = 'All';

    $scope.getstatus = function(status) {
        $scope.show = status;
    }

    $scope.geturl = function(x) {
        $scope.user = x;
    };

});


dataFeed.directive("myDisplay", function() {
    return {
        restrict: 'EA',
        templateUrl: "templates/data.html",
        scope: {
            display: "=display",
            user: '=',
            show: "="

        },
        controller: 'displayController',
        link: function(scope, element, attr) {
            scope.$watch(attr.user, function(value) {});

        }

    }

}).controller('displayController', function($scope, $element, $attrs) {

    $scope.sortableOptions = {
        containment: "parent",
        cursor: "move",
        tolerance: "pointer"
    }
});


//navigation
$(function() {
    $(".nav-toggle").click(function(e) {
        $(this).toggleClass('collapse');
        $("div.menu ul").toggleClass("collapse");
    });

});
